"""Generates a table via the Method of Differences"""

__version__ = '1.2'

import dataclasses

from result import Ok, Err, Result
import sympy
from sympy import latex
from typing import List

# All functions used in the method of differences are of a single variable. We expect
# the variable to be `x`.
VAR = sympy.Symbol('x')

# Maximum number of degrees to expand series (such as sin, cos)
EXPANSION_LIMIT = 10


@dataclasses.dataclass
class TableMeta:

    """f(x=initial) first row in table"""
    initial: float = 0.0

    """delta in x between rows (must be non-zero)"""
    step: float = 1.0

    """number of rows for generated table (must be positive integer)"""
    rows: int = 100

    def __post_init__(self):
        if self.step == 0:
            raise ValueError('step must be non-zero')
        if self.rows < 1:
            raise ValueError('rows must be positive')


@dataclasses.dataclass
class Explanation:
    # NB: We are using LaTeX as the portable syntax instead of MathML because
    # sympy renders deprecated elements (e.g. mfenced) and the GitHub issue
    # implies mathml maintenance is low on their priorities.

    """Initial expression for desired table (LaTeX)"""
    expression: str

    """Polynomial version of expression (e.g. taylor series expansion) (LaTeX)"""
    polynomial: str

    """Polynomial and derivatives evaluated at f(initial + step) - f(initial)"""
    initial_values: List[List[float]]

    """Table with intermediate and final results calculated via method of differences"""
    table: List[List[float]]

    """Original polynomial evaluated at f(x)"""
    check: List[float]


def parse_str(expr: str) -> Result[sympy.Expr, Exception]:
    """
    Parse a mathematical expression of a single variable (x) into a sympy Expression (Expr), if able.

    :param expr:
    :return:
    """
    try:
        e: sympy.Expr = sympy.parse_expr(expr, evaluate=False)

        if VAR not in e.free_symbols:
            return Err(ValueError('Required variable `x` not found in expression'))

        unexpected = e.free_symbols.difference({VAR})
        if len(unexpected) > 0:
            return Err(ValueError(f'Unexpected variables {unexpected} found in expression'))

        return Ok(e)
    except Exception as exc:
        return Err(exc)


def to_polynomial(expr: sympy.Expr) -> Result[sympy.Poly, Exception]:
    """
    Convert an expression (produced via parse_str) to a polynomial expression

    :param expr:
    :return:
    """
    try:
        expanded: sympy.Expr = expr.series(x=VAR, n=EXPANSION_LIMIT)
        expanded = expanded.removeO()
        poly: sympy.Poly | None = expanded.as_poly(VAR)

        if poly is None:
            return Err(ValueError('Expression could not be converted to a polynomial'))

        if poly.degree(VAR) > EXPANSION_LIMIT:
            return Err(ValueError(f'Expression must have degree <= {EXPANSION_LIMIT}'))

        return Ok(poly)
    except Exception as exc:
        return Err(exc)


def check_column(expr: sympy.Expr, meta: TableMeta) -> sympy.Array:
    """
    Compute a [meta.rows, 1] 'check' column by evaluating the polynomial poly at points
    initial and then successively increasing the step. This provides a way to
    verify the method of differences has yielded the same result (or very similarly).

    :param expr: expression to be evaluated
    :param meta: TableMeta
    :return:
    """
    return sympy.Array([expr.subs({VAR: meta.initial + meta.step * x}).evalf() for x in range(meta.rows)])


def initial_values(poly: sympy.Poly, meta: TableMeta) -> sympy.Array:
    """
    Compute a table of initial values via the simple differences method

    The initial values table with have the first column populated by evaluating
    the poly at the initial value and each step. Then, successive columns
    will be computed by the differences in their left and left-below values.
    Matrix values not computed will be zero.

    :param poly: polynomial input
    :param meta: metadata for the table
    :return: a NxN flipped upper triangular matrix
    """
    # rows go from meta.initial by meta.step; columns are f(x), D^1, D^2, ..., D^(poly.degree)
    n = int(poly.degree(VAR)) + 1
    table = sympy.MutableDenseNDimArray.zeros(n, n)

    # populate f(x) column
    for i in range(n):
        table[i, 0] = poly.eval(meta.initial + meta.step * i)

    # populate upper-left triangle of simple differences
    for j in range(1, n):
        for i in range(0, n - j):
            table[i, j] = table[i + 1, j - 1] - table[i, j - 1]

    return table.as_immutable()


def method_of_differences(initial_values: sympy.Array, meta: TableMeta) -> sympy.Array:
    """

    :param initial_values:
    :param meta:
    :return:
    """
    shape = (meta.rows, int(initial_values.shape[1]))
    last_col_idx = shape[1] - 1  # inclusive

    table = sympy.MutableDenseNDimArray.zeros(shape[0], shape[1])

    # set top row to initial value
    for j in range(0, shape[1]):
        table[0, j] = initial_values[0, j]

    # populate final column to the constant value
    constant = initial_values[0, last_col_idx]
    for row in range(1, meta.rows):
        table[row, last_col_idx] = constant

    # populate rest of table via addition
    for row in range(1, meta.rows):
        for col in range(last_col_idx - 1, -1, -1):
            table[row, col] = table[row - 1, col + 1] + table[row - 1, col]

    return table.as_immutable()


def explain_method(expr: str, meta: TableMeta) -> Result[Explanation, Exception]:
    """
    Build an explanation for how expr will be computed via the Method of Differences

    :param expr: string representation of the mathematical expression (SymPy format)
    :param meta: control the output format
    :return: either a populated Explanation or an exception if something is not computable
    """
    rexpression = parse_str(expr)
    rpolynomial = rexpression.and_then(to_polynomial)

    if rpolynomial.is_err():
        return rpolynomial
    else:
        expression = rexpression.unwrap()
        polynomial = rpolynomial.unwrap()
        ivs = initial_values(polynomial, meta)
        table = method_of_differences(ivs, meta)
        check = check_column(expression, meta)

        return Ok(Explanation(
            latex(expression),
            latex(polynomial.expr),
            ivs.tolist(),
            table.tolist(),
            check.tolist()))
