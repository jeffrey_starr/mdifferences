import unittest

import sympy
import mdifferences


class TableConstruction(unittest.TestCase):
    def compute_table(self, expr: str, meta: mdifferences.TableMeta):
        poly = mdifferences.parse_str(expr).and_then(mdifferences.to_polynomial).ok()

        ivs = mdifferences.initial_values(poly, meta)
        return mdifferences.method_of_differences(ivs, meta)

    def test_babbage_simple_case_table(self):
        expr = 'x**2 + x + 41'
        meta = mdifferences.TableMeta(1, 1, 4)
        table = self.compute_table(expr, meta)

        self.assertEqual(sympy.Array([43, 4, 2]), table[0])
        self.assertEqual(sympy.Array([47, 6, 2]), table[1])
        self.assertEqual(sympy.Array([53, 8, 2]), table[2])
        self.assertEqual(sympy.Array([61, 10, 2]), table[3])

    def test_cube_table(self):
        expr = 'x**3'
        meta = mdifferences.TableMeta(0, 1, 5)
        table = self.compute_table(expr, meta)

        self.assertEqual(sympy.Array([0, 1, 6, 6]), table[0])
        self.assertEqual(sympy.Array([1, 7, 12, 6]), table[1])
        self.assertEqual(sympy.Array([8, 19, 18, 6]), table[2])
        self.assertEqual(sympy.Array([27, 37, 24, 6]), table[3])
        self.assertEqual(sympy.Array([64, 61, 30, 6]), table[4])


class ExplainMethod(unittest.TestCase):

    def test_babbage_simple_case(self):
        meta = mdifferences.TableMeta(rows=4)
        expl = mdifferences.explain_method('x**2 + x + 41', meta).ok()

        self.assertEqual('x^{2} + x + 41', expl.expression)
        self.assertEqual('x^{2} + x + 41', expl.polynomial)

        self.assertEqual([[41, 2, 2], [43, 4, 0], [47, 0, 0]], expl.initial_values)

        self.assertEqual([41, 2, 2], expl.table[0])
        self.assertEqual([43, 4, 2], expl.table[1])
        self.assertEqual([47, 6, 2], expl.table[2])
        self.assertEqual([53, 8, 2], expl.table[3])

    def test_exception_no_x(self):
        meta = mdifferences.TableMeta()
        expl = mdifferences.explain_method('y**2', meta)

        self.assertEqual(ValueError, type(expl.err()))

    def test_exception_no_polynomial_form(self):
        meta = mdifferences.TableMeta()
        expl = mdifferences.explain_method('ln(x)', meta)

        self.assertEqual(ValueError, type(expl.err()))


class TableMetaValidation(unittest.TestCase):

    def test_step_must_be_nonzero(self):
        self.assertRaises(ValueError, lambda: mdifferences.TableMeta(step=0))

    def test_rows_must_be_positive(self):
        self.assertRaises(ValueError, lambda: mdifferences.TableMeta(rows=0))
        self.assertRaises(ValueError, lambda: mdifferences.TableMeta(rows=-1))


class CheckColumn(unittest.TestCase):

    def test_cubes(self):
        expr = sympy.parse_expr('x**3')
        meta = mdifferences.TableMeta(1, 1, 5)
        actual = mdifferences.check_column(expr, meta)
        expected = sympy.Array([1, 8, 27, 64, 125])
        self.assertEqual(expected, actual)


class InitialValues(unittest.TestCase):

    def test_cubes(self):
        poly = sympy.parse_expr('x**3').as_poly()
        meta = mdifferences.TableMeta(1, 1, 5)
        actual = mdifferences.initial_values(poly, meta)
        expected = sympy.Array([[1, 7, 12, 6],
                                [8, 19, 18, 0],
                                [27, 37, 0, 0],
                                [64, 0, 0, 0]])
        self.assertEqual(expected, actual)

    def test_cubes_zero_start(self):
        poly = sympy.parse_expr('x**3').as_poly()
        meta = mdifferences.TableMeta(0, 1, 5)
        actual = mdifferences.initial_values(poly, meta)
        expected = sympy.Array([[0, 1, 6, 6],
                                [1, 7, 12, 0],
                                [8, 19, 0, 0],
                                [27, 0, 0, 0]])
        self.assertEqual(expected, actual)

    def test_simple_case(self):
        poly = sympy.parse_expr('x**2 + x + 41').as_poly()
        meta = mdifferences.TableMeta(0, 1, 5)
        actual = mdifferences.initial_values(poly, meta)
        expected = sympy.Array([[41, 2, 2],
                                [43, 4, 0],
                                [47, 0, 0]])
        self.assertEqual(expected, actual)


if __name__ == '__main__':  # pragma: no cover
    unittest.main()
